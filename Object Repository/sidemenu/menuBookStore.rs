<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>menuBookStore</name>
   <tag></tag>
   <elementGuidId>9d57118b-f7a3-4b8e-b91d-20dde076d805</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.element-list.collapse.show > ul.menu-list > #item-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='item-2'])[5]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
